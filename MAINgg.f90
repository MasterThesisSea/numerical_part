PROGRAM main
   USE my_fxn
   USE MC_VEGAS
   IMPLICIT NONE

   INTEGER, PARAMETER        :: NDIM = 7
   INTEGER                   :: loopdelta, loopCMSE
   REAL(KIND(0D0))           :: Center_Mass_System_Energy
   REAL(KIND(0d0))           :: avgi_gg, sigma_gg, sd, chi2a
   CHARACTER(LEN=40)         :: Tablefile
   INTEGER , DIMENSION(2:6)  :: deltalist
   REAL(KIND(0D0)), DIMENSION(2:6) ::MDlist
   REAL(KIND(0D0)), DIMENSION(1:4) :: CMSElist
   CHARACTER(len=20), DIMENSION(2:6)    :: filename
   !CHARACTER(LEN = 128)      :: arg
   DATA Tablefile/'CT14LL.pds'/
   CALL SetCT14(Tablefile)
   deltalist = [2, 3, 4, 5, 6]
   CMSElist=[7d3, 8d3, 13d3, 14d3]
   filename = ['result/ggd2.txt', 'result/ggd3.txt', 'result/ggd4.txt', &
               'result/ggd5.txt', 'result/ggd6.txt']
   MDlist = [2.3d3, 2.35d3, 2.4d3, 2.45d3, 2.5d3]


   nd = NDIM
   pseudorapidity = 2d0

   do loopdelta = 2, 6
     delta = deltalist(loopdelta)
     M_D = MDlist(loopdelta)
     OPEN(110,file = filename(loopdelta), position = 'append', status='unknown')

     do loopCMSE = 1,4
       Center_Mass_System_Energy = CMSElist(loopCMSE)
       S = Center_Mass_System_Energy ** 2
       CALL vegas(NDIM,fxn_2,avgi_gg,sd,chi2a)
       sigma_gg = avgi_gg * 3.894 * 10 ** 8
       print *,  delta, 'CMSE= ', Center_Mass_System_Energy, sigma_gg, 'pb'
       WRITE(110,*) Center_Mass_System_Energy , sigma_gg
     end do
     close(110)
   end do
end program main
