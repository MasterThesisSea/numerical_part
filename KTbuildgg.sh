#!/bin/sh
rm -rf *.mod
rm -rf *.o
rm -rf ./calc
#rm DATAgg.txt

gfortran -g -fbacktrace -fcheck=all -Wall -c CT14Pdf.for
gfortran -g -fbacktrace -fcheck=all -Wall -c KTFXNgg.f90
gfortran -g -fbacktrace -fcheck=all -Wall -c MC_VEGAS.f90
gfortran -g -fbacktrace -fcheck=all -Wall -c KTMAINgg.f90

gfortran  -g -fbacktrace -fcheck=all -Wall -o calc KTMAINgg.o CT14Pdf.o KTFXNgg.o MC_VEGAS.o
rm -rf *.mod
rm -rf *.o
