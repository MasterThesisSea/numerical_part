#!/bin/sh
rm -rf *.mod
rm -rf *.o
rm -rf ./calc
#rm DATAgg.txt

gfortran -g -fbacktrace -fcheck=all -Wall -c CT14Pdf.for
gfortran -g -fbacktrace -fcheck=all -Wall -c M_DFXNgg.f90
gfortran -g -fbacktrace -fcheck=all -Wall -c MC_VEGAS.f90
gfortran -g -fbacktrace -fcheck=all -Wall -c M_DMAINgg.f90

gfortran  -g -fbacktrace -fcheck=all -Wall -o calc M_DMAINgg.o CT14Pdf.o M_DFXNgg.o MC_VEGAS.o
rm -rf *.mod
rm -rf *.o
