PROGRAM main
    USE my_fxn
    USE MC_VEGAS
    IMPLICIT NONE

    INTEGER, PARAMETER        :: NDIM = 6
    INTEGER                   :: loopdelta, i
    REAL(KIND(0D0))           :: Center_Mass_System_Energy, &
                                 interval
    REAL(KIND(0d0))           :: avgi_qq, sigma_qq, sd, chi2a
    CHARACTER(LEN=40)         :: Tablefile
    INTEGER , DIMENSION(1:3)  :: deltalist
    REAL(KIND(0D0)), DIMENSION(1:3) ::MDlist
    REAL(KIND(0D0)), DIMENSION(1:4) :: CMSElist
    CHARACTER(len=20), DIMENSION(1:3)    :: filename
    !CHARACTER(LEN = 128)      :: arg
    DATA Tablefile/'CT14LL.pds'/
    CALL SetCT14(Tablefile)
    deltalist = [2,  4,  6]
    CMSElist=[7d3, 8d3, 13d3, 14d3]
    filename = ['result/KTqqS7d2.txt',  'result/KTqqS7d4.txt', 'result/KTqqS7d6.txt']
    MDlist = [2.3d3, 2.4d3,  2.5d3]


    nd = NDIM
    pseudorapidity = 2d0
    Center_Mass_System_Energy = CMSElist(1)
    S = Center_Mass_System_Energy ** 2


    do loopdelta = 1, 3
      M_D = MDlist(loopdelta)
      delta = deltalist(loopdelta)
      k_T = 3d2
      interval = (2d3-3d2)/50

      OPEN(110,file = filename(loopdelta), position = 'append', status='unknown')

        do i = 0, 50
          CALL vegas(NDIM,fxn_1,avgi_qq,sd,chi2a)
          sigma_qq = avgi_qq * 3.894 * 10 ** 8
          print *,  k_T, sigma_qq, 'pb/GeV'
          WRITE(110,*) k_T , sigma_qq
          k_T = k_T + interval
        end do
      close(110)
    end do
 end program main
