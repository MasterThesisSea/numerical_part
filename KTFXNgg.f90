MODULE my_fxn
   IMPLICIT NONE
   PRIVATE
   PUBLIC ::  fxn_2, &
              nd, S, delta, pseudorapidity, M_D, K_T


   REAL(KIND(0D0))                 :: S                            ! S = (P1+P2)^2 which P1, P2 is four vector of protons
   REAL(KIND(0D0)), PARAMETER      :: g_s = 1.22d0
   REAL(KIND(0d0))                 :: M_D
   REAL(KIND(0d0))                 :: pseudorapidity               ! Plack energy in D dimensional
   INTEGER                         :: nd                           ! nd is the number of integration
   INTEGER                         :: delta                        ! delta is the number of extra dimension
   REAL(KIND(0D0)), PARAMETER      :: m = 173.2d0                  ! The mass of top quark
   REAL(KIND(0D0)), PARAMETER      :: pi=3.14159d0
   REAL(KIND(0D0)), EXTERNAL       :: CT14pdf
   REAL(KIND(0d0)) :: s12                                          ! s12 = x1*x2*S, which p1, p2 is total energy in parton reference frame.
   INTEGER         :: i                                            ! i is loop index for subrountines
   REAL(KIND(0d0)) :: k_T
   CONTAINS
      FUNCTION jacobian( upper, lower) RESULT(jfactor)
         IMPLICIT NONE
         REAL(KIND(0D0)), DIMENSION(1:nd) :: upper, lower
         REAL(KIND(0D0))  :: jfactor

         jfactor = 1d0
         DO i = 1, nd
            jfactor = jfactor * (upper(i) - lower(i))
         END DO
      END FUNCTION jacobian



      FUNCTION dot_vec(p,q) RESULT(fourvectordot)
         IMPLICIT NONE
         REAL(KIND(0d0)) :: fourvectordot
         REAL(KIND(0D0)), DIMENSION(0:3) :: p,q

         fourvectordot = p(0) * q(0)
         DO i = 1, 3
            fourvectordot = fourvectordot - p(i) * q(i)
         END DO
      END FUNCTION dot_vec

      FUNCTION fxn_2(z, wgt) RESULT(fxn_gg)
         IMPLICIT NONE
         REAL(KIND(0d0)) :: wgt, sunn
         REAL(KIND(0D0)), DIMENSION(1:nd) :: z
         REAL(KIND(0D0)) :: gm, p3_0, k_0, cos_theta, eta, x1,x2
         REAL(KIND(0d0)) :: tau_0
         REAL(KIND(0D0)), DIMENSION(0:3) :: k1, k2, p3, p4, k
         REAL(KIND(0D0)) :: s13, s14, s23, s24
         REAL(KIND(0d0)) :: sin_theta,cos_eta, sin_eta, cos_ksi, sin_ksi, &
                            p3_v, p4_v, k_v , &
                            sigma, tau, m_plus, m_minus
         REAL(KIND(0d0)) :: part_gg,fxn_gg
         REAL(KIND(0d0)) :: p3_0_max, k_0_max, cos_theta_max, eta_max, gm_max, x1_max, x2_max, &
                            p3_0_min, k_0_min, cos_theta_min, eta_min, gm_min, x1_min, x2_min
         REAL(KIND(0D0)), DIMENSION(1:nd) :: upper, lower
         REAL(KIND(0d0)) :: jfactor, phi, UnitSphereSurface
         REAL(KIND(0D0))    :: G_QCDFactorazationScale
         sunn = 3d0 ! usage: Bingo include sunn
         wgt = 0d0
         G_QCDFactorazationScale=M_D

         gm_max = M_D
         gm_min = 5d2 ! crucial!!! since it should comparable with m_t
         gm= (gm_max-gm_min)*z(1) + gm_min

         tau_0 = (2*m +gm )**2/S

         eta_max = 2*pi
         eta_min = 0
         eta = (eta_max-eta_min)*z(2)+eta_min

         cos_eta = COS(eta)
         sin_eta = SIN(eta)

         cos_theta_max = 1
         cos_theta_min = -1
         cos_theta = (cos_theta_max-cos_theta_min)*z(3)+cos_theta_min

         sin_theta = SQRT(1-cos_theta**2)


         IF(DABS(DLOG(DTAN(DACOS(cos_theta)/2))) > pseudorapidity )THEN
           fxn_gg = 0
           RETURN
         ELSE
         ENDIF

         x1_max = 1
         x1_min = tau_0
         x1 = (x1_max-x1_min)*z(4) + x1_min

         x2_max = 1
         x2_min = tau_0/x1
         x2 = (x2_max-x2_min)*z(5)+x2_min

         s12 = x1*x2 * S

         k_0_max = SQRT(s12)/2 - ((2*m)**2-gm**2)/(2*SQRT(s12))
         k_0_min = gm

         k_v = k_T/sin_theta
         k_0 = SQRT(k_v**2 + gm**2)

         IF(k_0<SQRT(gm**2+(100/sin_theta)**2))THEN
           fxn_gg = 0
           RETURN
         ELSEIF(k_0>k_0_max)THEN
           fxn_gg = 0
           RETURN
         ELSE
         ENDIF


         sigma = SQRT(s12)-k_0
         tau = sigma**2 - k_v**2
         m_plus = 2*m
         m_minus = 0

         p3_0_max = 1/(2*tau)*(sigma*(tau+m_plus*m_minus)+k_v*SQRT((tau-m_plus**2)*(tau-m_minus**2)))
         p3_0_min = 1/(2*tau)*(sigma*(tau+m_plus*m_minus)-k_v-SQRT((tau-m_plus**2)*(tau-m_minus**2)))
         p3_0 = (p3_0_max-p3_0_min)*z(6)+p3_0_min
      !   IF(p3_0 < 1000)Then
      !     fxn_gg = 0
      !     RETURN
      !   ELSE
      !   ENDIF

         p3_v = SQRT(p3_0**2-m**2)
         p4_v = SQRT((sqrt(s12)-k_0-p3_0)**2-m**2)


         cos_ksi = (p4_v**2-p3_v**2-k_v**2)/(2*p3_v*k_v)
         sin_ksi = SQRT(1-cos_ksi**2)

         upper = [gm_max, eta_max, cos_theta_max, x1_max, x2_max, p3_0_max]
         lower = [gm_min, eta_min, cos_theta_min, x1_min, x2_min, p3_0_min]
         jfactor = jacobian(upper, lower)




         k1 = [SQRT(s12)/2d0,0d0,0d0, SQRT(s12)/2d0]
         k2 = [SQRT(s12)/2d0,0d0,0d0, -SQRT(s12)/2d0]
         p3 = [p3_0, p3_v*(cos_theta*cos_eta*sin_ksi+sin_theta*cos_ksi), &
               p3_v* sin_eta*sin_ksi, p3_v*( cos_theta*cos_ksi-sin_theta*cos_eta*sin_ksi)]
         k = [k_0, k_v*sin_theta, 0d0, k_v*cos_theta]

         DO i = 1, 3
         p4(i)  = 0 - p3(i) - k(i)
         END DO
         p4(0) = sqrt(s12) - p3_0-k_0

         s13 = m**2- 2*dot_vec(k1,p3)
         s14 = m**2- 2*dot_vec(k1,p4)
         s23 = m**2- 2*dot_vec(k2,p3)
         s24 = m**2- 2*dot_vec(k2,p4)

         INCLUDE "input/bingo.m"

         part_gg = CT14Pdf(0, x1, G_QCDFactorazationScale**2)* &
         CT14Pdf(0, x2, G_QCDFactorazationScale**2) * part_gg

         phi = 1/(8*(2*pi)**4) * 1/(2*s12)

         UnitSphereSurface = 2*pi**(delta/2)/gamma(delta/2d0)
         fxn_gg = g_s**4*UnitSphereSurface*1/M_D**(2+delta)*gm**(delta-1)*phi*jfactor*part_gg
      END FUNCTION fxn_2
END MODULE my_fxn
