MODULE my_fxn
   IMPLICIT NONE
   PRIVATE
   PUBLIC ::  fxn_1
   PUBLIC ::  nd, S, delta, pseudorapidity,M_D

   REAL(KIND(0D0))                 :: S                             ! S = (P1+P2)^2 which P1, P2 is four vector of protons
   REAL(KIND(0D0)), PARAMETER      :: g_s = 1.22d0
   REAL(KIND(0d0))                 :: M_D
   REAL(KIND(0d0))                 :: pseudorapidity                ! Plack energy in D dimensional
   INTEGER                         :: nd,  delta                    ! delta is the number of extra dimension
   REAL(KIND(0D0)), PARAMETER      :: m = 173.2d0                   ! The mass of top quark
   REAL(KIND(0D0)), PARAMETER      :: pi=3.14159d0
   REAL(KIND(0D0)), EXTERNAL       :: CT14pdf
   REAL(KIND(0d0)) :: s12                                           ! s12 = x1*x2*S, which p1, p2 is total energy in parton reference frame.
   INTEGER         :: i                                             ! i is loop index for subrountines
   CONTAINS
      FUNCTION jacobian( upper, lower) RESULT(jfactor)
         IMPLICIT NONE
         REAL(KIND(0D0)), DIMENSION(1:nd) :: upper, lower
         REAL(KIND(0D0))  :: jfactor

         jfactor = 1d0
         DO i = 1, nd
            jfactor = jfactor * (upper(i) - lower(i))
         END DO
      END FUNCTION jacobian



      FUNCTION dot_vec(p,q) RESULT(fourvectordot)
         IMPLICIT NONE
         REAL(KIND(0d0)) :: fourvectordot
         REAL(KIND(0D0)), DIMENSION(0:3) :: p,q

         fourvectordot = p(0) * q(0)
         DO i = 1, 3
            fourvectordot = fourvectordot - p(i) * q(i)
         END DO
      END FUNCTION dot_vec

      FUNCTION fxn_1(z, wgt) RESULT(fxn_qq)
         IMPLICIT NONE
         REAL(KIND(0d0)) :: wgt
         REAL(KIND(0D0)), DIMENSION(1:nd) :: z
         REAL(KIND(0D0)) :: gm, p3_0, k_0, cos_theta, eta, x1,x2
         REAL(KIND(0d0)) :: tau_0
         REAL(KIND(0D0)), DIMENSION(0:3) :: k1, k2, p3, p4, k
         REAL(KIND(0D0)) :: s13, s14, s23, s24
         REAL(KIND(0d0)) :: sin_theta,cos_eta, sin_eta, cos_ksi, sin_ksi, &
                            p3_v, p4_v, k_v , &
                            sigma, tau, m_plus, m_minus
         REAL(KIND(0d0)) :: part1_qq,part_qq,fxn_qq
         REAL(KIND(0d0)) :: p3_0_max, k_0_max, cos_theta_max, eta_max, gm_max, x1_max, x2_max, &
                            p3_0_min, k_0_min, cos_theta_min, eta_min, gm_min, x1_min, x2_min
         REAL(KIND(0D0)), DIMENSION(1:nd) :: upper, lower
         REAL(KIND(0d0)) :: jfactor, phi, UnitSphereSurface
         REAL(KIND(0D0))    :: G_QCDFactorazationScale
         wgt = 0
         G_QCDFactorazationScale=M_D

         gm_max = M_D
         gm_min = 5d2 ! crucial!!! since it should comparable with m_t
         gm= (gm_max-gm_min)*z(1) + gm_min

         tau_0 = (2*m +gm )**2/S

         eta_max = 2*pi
         eta_min = 0
         eta = (eta_max-eta_min)*z(2)+eta_min

         cos_theta_max = 1
         cos_theta_min = -1
         cos_theta = (cos_theta_max-cos_theta_min)*z(3)+cos_theta_min

         IF(DABS(DLOG(DTAN(DACOS(cos_theta)/2))) > pseudorapidity )THEN
           fxn_qq = 0
           RETURN
         ELSE
         ENDIF

         x1_max = 1
         x1_min = tau_0
         x1 = (x1_max-x1_min)*z(4) + x1_min

         x2_max = 1
         x2_min = tau_0/x1
         x2 = (x2_max-x2_min)*z(5)+x2_min

         s12 = x1*x2 * S

         k_0_max = SQRT(s12)/2 - ((2*m)**2-gm**2)/(2*SQRT(s12))
         k_0_min = gm
         k_0 = (k_0_max-k_0_min)*z(6)+k_0_min

         k_v = SQRT(k_0**2-gm**2)

         IF(k_v*sqrt(1-cos_theta**2)<100)THEN
           fxn_qq = 0
           RETURN
         ELSE
         ENDIF

         sigma = SQRT(s12)-k_0
         tau = sigma**2 - k_v**2
         m_plus = 2*m
         m_minus = 0

         p3_0_max = 1/(2*tau)*(sigma*(tau+m_plus*m_minus)+k_v*SQRT((tau-m_plus**2)*(tau-m_minus**2)))
         p3_0_min = 1/(2*tau)*(sigma*(tau+m_plus*m_minus)-k_v-SQRT((tau-m_plus**2)*(tau-m_minus**2)))
         p3_0 = (p3_0_max-p3_0_min)*z(7)+p3_0_min


         p3_v = SQRT(p3_0**2-m**2)
         p4_v = SQRT((sqrt(s12)-k_0-p3_0)**2-m**2)

         upper = [gm_max, eta_max, cos_theta_max, x1_max, x2_max, k_0_max, p3_0_max]
         lower = [gm_min, eta_min, cos_theta_min, x1_min, x2_min, k_0_min, p3_0_min]
         jfactor = jacobian(upper, lower)

         sin_theta = SQRT(1-cos_theta**2)
         cos_eta = COS(eta)
         sin_eta = SIN(eta)
         cos_ksi = (p4_v**2-p3_v**2-k_v**2)/(2*p3_v*k_v)
         sin_ksi = SQRT(1-cos_ksi**2)

         k1 = [SQRT(s12)/2d0,0d0,0d0, SQRT(s12)/2d0]
         k2 = [SQRT(s12)/2d0,0d0,0d0, -SQRT(s12)/2d0]
         p3 = [p3_0, p3_v*(cos_theta*cos_eta*sin_ksi+sin_theta*cos_ksi), &
               p3_v* sin_eta*sin_ksi, p3_v*( cos_theta*cos_ksi-sin_theta*cos_eta*sin_ksi)]
      !  IF(SQRT(p3(1)**2+p3(2)**2 < )
      !    fxn_gg =0
      !      RETURN
      !  ELSEIF
      !  END
         k = [k_0, k_v*sin_theta, 0d0, k_v*cos_theta]
         !  IF(SQRT(p4(1)**2+p4(2)**2 < )
         !    fxn_gg =0
         !      RETURN
         !  ELSEIF
         !  END
         DO i = 1, 3
         p4(i)  = 0 - p3(i) - k(i)
         END DO
         p4(0) = sqrt(s12) - p3_0-k_0

         s13 = m**2- 2*dot_vec(k1,p3)
         s14 = m**2- 2*dot_vec(k1,p4)
         s23 = m**2- 2*dot_vec(k2,p3)
         s24 = m**2- 2*dot_vec(k2,p4)

         INCLUDE "input/juicy.m"
         part1_qq = 0d0
         DO i = 1, 5
            part1_qq = part1_qq+CT14Pdf(i, x1, G_QCDFactorazationScale**2)* &
            CT14Pdf(-i, x2, G_QCDFactorazationScale**2)*part_qq
         END DO

         phi = 1/(8*(2*pi)**4) * 1/(2*s12)
         UnitSphereSurface = 2*pi**(delta/2)/gamma(delta/2d0)

         fxn_qq = g_s**4*UnitSphereSurface*1/M_D**(2+delta)*gm**(delta-1)*phi*jfactor*part1_qq
      END FUNCTION fxn_1
END MODULE my_fxn
