!test \int_0^1 dx \int_0^x dy xy
module toy
  implicit none
  private
  public:: fd
  contains
    function fd(z,wgt) result(fxn)
      implicit none
      real(kind(0d0)), dimension(1:2) :: z
      real(kind(0d0)), dimension(1:2) :: minlimit,maxlimit
      real(kind(0d0)) :: wgt, jacobian, fxn
      wgt = 0d0
      minlimit(1) = 0
      maxlimit(1) = 1
      z(1) = (maxlimit(1) - minlimit(1))*z(1)+minlimit(1)
      minlimit(2) = 0
      maxlimit(2) = z(1)
      z(2) = (maxlimit(2) - minlimit(2))*z(2)+minlimit(2)
      jacobian = (maxlimit(2) - minlimit(2))*(maxlimit(1) - minlimit(1))
      fxn = jacobian* z(1)*z(2)
    end function fd
end module toy

program test_vegas_integration_order
  use toy
  use MC_VEGAS
  implicit none
  real(kind(0d0)):: outcome, sd, chi2a

  call vegas(2,fd,outcome,sd,chi2a)
  print *, outcome
end program test_vegas_integration_order



PROGRAM main
   USE my_fxn
   USE MC_VEGAS
   IMPLICIT NONE

   INTEGER, PARAMETER        :: NDIM = 7
   REAL(KIND(0D0))           :: Center_Mass_System_Energy
   REAL(KIND(0d0))           :: avgi_gg, sigma_gg, sd, chi2a
   REAL(KIND(0d0))           :: interval
   INTEGER                   :: j
   CHARACTER(LEN=40)         :: Tablefile
   CHARACTER(LEN=128)        :: arg
   DATA Tablefile/'CT14LL.pds'/
   CALL SetCT14(Tablefile)

   nd = NDIM
   Center_Mass_System_Energy = 14d3
   S = Center_Mass_System_Energy ** 2
   delta = 4
   pseudorapidity = 2d0

   M_D = 2.3d3
   interval = (10d3-2.3d3)/50

   CALL GET_COMMAND_ARGUMENT(1, arg)
   READ(arg,*) j

   M_D = M_D + j*interval

   CALL vegas(NDIM,fxn_2,avgi_gg,sd,chi2a)
   sigma_gg = avgi_gg * 3.894 * 10 ** 8
   print *, M_D,  sigma_gg, 'pb'

   OPEN(110,file = 'result/ggS14d4', position = 'append', status='unknown')
   WRITE(110,*) M_D , sigma_gg
   close(110)
end program main
