#!/bin/sh
rm -rf *.mod
rm -rf *.o
rm -rf ./calc
rm DATAqq.txt

gfortran -g -fbacktrace -fcheck=all -Wall -c CT14Pdf.for
gfortran -g -fbacktrace -fcheck=all -Wall -c M_DFXNqq.f90
gfortran -g -fbacktrace -fcheck=all -Wall -c MC_VEGAS.f90
gfortran -g -fbacktrace -fcheck=all -Wall -c M_DMAINqq.f90

gfortran  -g -fbacktrace -fcheck=all -Wall -o calc M_DMAINqq.o CT14Pdf.o M_DFXNqq.o MC_VEGAS.o
rm -rf *.mod
rm -rf *.o
