PROGRAM main
    USE my_fxn
    USE MC_VEGAS
    IMPLICIT NONE

    INTEGER, PARAMETER        :: NDIM = 7
    INTEGER                   :: loopdelta, loopCMSE
    REAL(KIND(0D0))           :: Center_Mass_System_Energy
    REAL(KIND(0d0))           :: avgi_qq, sigma_qq, sd, chi2a
    CHARACTER(LEN=40)         :: Tablefile
    INTEGER , DIMENSION(2:6)  :: deltalist
    REAL(KIND(0D0)), DIMENSION(2:6) ::MDlist
    REAL(KIND(0D0)), DIMENSION(1:4) :: CMSElist
    CHARACTER(len=20), DIMENSION(2:6)    :: filename
    !CHARACTER(LEN = 128)      :: arg
    DATA Tablefile/'CT14LL.pds'/
    CALL SetCT14(Tablefile)
    deltalist = [2, 3, 4, 5, 6]
    CMSElist=[7d3, 8d3, 13d3, 14d3]
    filename = ['qqd2.txt', 'qqd3.txt', 'qqd4.txt', 'qqd5.txt', 'qqd6.txt']
    MDlist = [2.3d3, 2.35d3, 2.4d3, 2.45d3, 2.5d3]


    nd = NDIM
    delta = 2
    pseudorapidity = 2d0

    do loopdelta = 2, 6
      delta = deltalist(loopdelta)
      M_D = MDlist(loopdelta)
      OPEN(loopdelta-1,file = filename(loopdelta), position = 'append', status='unknown')

      do loopCMSE = 1,4
        Center_Mass_System_Energy = CMSElist(loopCMSE)
        S = Center_Mass_System_Energy ** 2
        CALL vegas(NDIM,fxn_1,avgi_qq,sd,chi2a,2)
        sigma_qq = avgi_qq * 3.894 * 10 ** 5
        print *,  delta, 'CMSE= ', Center_Mass_System_Energy, sigma_qq, 'pb'
        WRITE(loopdelta-1,*) Center_Mass_System_Energy , sigma_qq
      end do
      close(loopdelta-1)
    end do
end program main
