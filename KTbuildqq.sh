#!/bin/sh
rm -rf *.mod
rm -rf *.o
rm -rf ./calc
rm DATAqq.txt

gfortran -g -fbacktrace -fcheck=all -Wall -c CT14Pdf.for
gfortran -g -fbacktrace -fcheck=all -Wall -c KTFXNqq.f90
gfortran -g -fbacktrace -fcheck=all -Wall -c MC_VEGAS.f90
gfortran -g -fbacktrace -fcheck=all -Wall -c KTMAINqq.f90

gfortran  -g -fbacktrace -fcheck=all -Wall -o calc KTMAINqq.o CT14Pdf.o KTFXNqq.o MC_VEGAS.o
rm -rf *.mod
rm -rf *.o
